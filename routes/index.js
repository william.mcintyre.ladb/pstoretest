var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  console.log(process.env.message)
  res.render('index', { title: 'Express', message: process.env.message });
});

module.exports = router;
